<?php

Route::get('', function(){
    return view('welcome');
});

// Autenticação
Route::auth();

// Admin
Route::group(['prefix' => 'admin', 'middleware' => 'auth', 'namespace' => 'Admin', 'as' => 'admin.'], function() {

    // Dashboard
    Route::get('', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);

    // Categories
    Route::group(['prefix' => 'categories', 'as' => 'categories.'], function() {
        Route::get('', ['as' => 'index', 'uses' => 'CategoriesController@index']);
        Route::get('create', ['as' => 'create', 'uses' => 'CategoriesController@create']);
        Route::post('store', ['as' => 'store', 'uses' => 'CategoriesController@store']);
        Route::get('edit/{id}', ['as' => 'edit', 'uses' => 'CategoriesController@edit'])->where('id', '[0-9]+');
        Route::post('update/{id}', ['as' => 'update', 'uses' => 'CategoriesController@update'])->where('id', '[0-9]+');
        Route::get('destroy/{id}', ['as' => 'destroy', 'uses' => 'CategoriesController@destroy'])->where('id', '[0-9]+');
    });

    // Posts
    Route::group(['prefix' => 'posts', 'as' => 'posts.'], function() {
        Route::get('', ['as' => 'index', 'uses' => 'PostsController@index']);
        Route::get('create', ['as' => 'create', 'uses' => 'PostsController@create']);
        Route::post('store', ['as' => 'store', 'uses' => 'PostsController@store']);
        Route::get('edit/{id}', ['as' => 'edit', 'uses' => 'PostsController@edit'])->where('id', '[0-9]+');
        Route::post('update/{id}', ['as' => 'update', 'uses' => 'PostsController@update'])->where('id', '[0-9]+');
        Route::get('destroy/{id}', ['as' => 'destroy', 'uses' => 'PostsController@destroy'])->where('id', '[0-9]+');
    });

});


