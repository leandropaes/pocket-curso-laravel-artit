<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CategoriesRequest;
use App\Models\Category;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    /**
     * @var Category
     */
    private $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function index()
    {
        $categories = $this->category->with('posts')->get();

        return view('admin.categories.index', compact('categories'));
    }

    public function create()
    {
        return view('admin.categories.create');
    }

    public function store(CategoriesRequest $request)
    {
        $this->category->create($request->all());

        return redirect()->route('admin.categories.index')->with('msg', 'Saved!');
    }

    public function edit($id)
    {
        $category = $this->category->find($id);

        return view('admin.categories.edit', compact('category'));
    }

    public function update(CategoriesRequest $request, $id)
    {
        $this->category->find($id)->update($request->all());

        return redirect()->route('admin.categories.index')->with('msg', 'Saved!');
    }

    public function destroy($id)
    {
        $this->category->destroy($id);

        return redirect()->route('admin.categories.index')->with('msg', 'Deleted!');
    }
}
