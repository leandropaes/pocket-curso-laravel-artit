<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PostsRequest;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Admin\PostService;

class PostsController extends Controller
{
    /**
     * @var Post
     */
    private $post;
    /**
     * @var Category
     */
    private $category;
    /**
     * @var PostService
     */
    private $postService;

    public function __construct(Post $post,Category $category, PostService $postService)
    {
        $this->post = $post;
        $this->category = $category;
        $this->postService = $postService;
    }

    public function index(Request $request)
    {
        $categories = $this->category->lists('name', 'id');

        $posts = $this->post
                      ->with('category')
                      ->byCategory($request->get('category'))
                      ->paginate(1);

        /*$posts = $this->post
                      ->joinFull()
                      ->byCategory($request->get('category'))
                      ->paginate(1);*/

        return view('admin.posts.index', compact('posts', 'categories', 'request'));
    }

    public function create()
    {
        $categories = $this->category->lists('name', 'id');

        return view('admin.posts.create', compact('categories'));
    }

    public function store(PostsRequest $request)
    {
        $this->postService->store($request->all());

        return redirect()->route('admin.posts.index')->with('msg', 'Saved!');
    }

    public function edit($id)
    {
        $categories = $this->category->lists('name', 'id');

        $post = $this->post->find($id);

        return view('admin.posts.edit', compact('post', 'categories'));
    }

    public function update(Request $request, $id)
    {
        $this->postService->update($request->all(), $id);

        return redirect()->route('admin.posts.index')->with('msg', 'Saved!');
    }

    public function destroy($id)
    {
        $this->post->destroy($id);

        return redirect()->route('admin.posts.index')->with('msg', 'Deleted!');
    }
}
