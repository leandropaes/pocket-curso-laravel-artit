<?php

namespace App\Services\Admin;

use App\Models\Post;

class PostService
{
    /**
     * @var Post
     */
    private $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function store(array $data)
    {
        return $this->post->create($data);
    }

    public function update(array $data, $id)
    {
        return $this->post->find($id)->update($data);
    }
}