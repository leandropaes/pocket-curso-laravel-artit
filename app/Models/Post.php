<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['category_id', 'title', 'text', 'image'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function scopeJoinFull($query)
    {
        return $query->selectRaw("
                        posts.*,
                        categories.name as category
                     ")
                     ->join('categories', 'categories.id', '=', 'posts.category_id')
                     ->orderBy('posts.title');
    }

    public function scopeByCategory($query, $categoryId = null)
    {
        return ($categoryId) ? $query->where('category_id', '=', $categoryId) : $query;
    }

    public function getCreatedAtAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }

    public function getFullNameAttribute()
    {
        return $this->title . ' ' . $this->category->name;
    }
}
