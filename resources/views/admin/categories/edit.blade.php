@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <h1>
                    Categories <small>\ Edit: {{ $category->name }}</small>
                </h1>
            </div>
        </div>

        <hr>

        @include('admin.utils.errors')

        <div class="row">
            <div class="col-sm-12">

                {!! Form::model($category, ['route' => ['admin.categories.update', $category->id], 'method' => 'post']) !!}

                    @include('admin.categories.partials.form')

                {!! Form::close() !!}

            </div>
        </div>

    </div>
@endsection
