@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <h1>
                    Categories
                    <a href="{{ route('admin.categories.create') }}" class="btn btn-primary pull-right">New Category</a>
                </h1>
            </div>
        </div>

        <hr>

        @include('admin.utils.msg')

        <div class="row">
            <div class="col-sm-12">

                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Posts Count</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse($categories as $c)
                            <tr>
                                <td width="1">{{ $c->id }}</td>
                                <td>{{ $c->name }}</td>
                                <td>{{ $c->posts->count() }}</td>
                                <td width="130">
                                    <a href="{{ route('admin.categories.edit', $c->id) }}" class="btn btn-sm btn-default">Edit</a>
                                    <a href="{{ route('admin.categories.destroy', $c->id) }}" class="btn btn-sm btn-danger">Remove</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3">No results</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>

            </div>
        </div>

    </div>
@endsection
