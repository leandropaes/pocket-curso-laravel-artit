<div class="form-group">
    {!! Form::label('name', 'Name') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
    <a href="{{ route('admin.categories.index') }}" class="btn btn-default">Cancel</a>
</div>