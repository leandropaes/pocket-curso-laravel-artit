@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <h1>
                    Categories <small>\ New</small>
                </h1>
            </div>
        </div>

        <hr>

        @include('admin.utils.errors')

        <div class="row">
            <div class="col-sm-12">

                {!! Form::open(['route' => 'admin.categories.store', 'method' => 'post']) !!}

                    @include('admin.categories.partials.form')

                {!! Form::close() !!}

            </div>
        </div>

    </div>
@endsection
