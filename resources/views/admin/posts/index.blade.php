@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <h1>
                    Posts
                    <a href="{{ route('admin.posts.create') }}" class="btn btn-primary pull-right">New Post</a>
                </h1>
            </div>
        </div>

        <hr>

        {!! Form::open(['route' => 'admin.posts.index', 'method' => 'get']) !!}
        <div class="well well-sm">
            <div class="row">
                <div class="col-sm-6">
                    {!! Form::select('category', $categories, $request->get('category'), ['class' => 'form-control']) !!}
                </div>
                <div class="col-sm-6">
                    {!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}
                </div>
            </div>
        </div>
        {!! Form::close() !!}

        @include('admin.utils.msg')

        <div class="row">
            <div class="col-sm-12">

                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Category</th>
                        <th>Date</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse($posts as $p)
                            <tr>
                                <td width="1">{{ $p->id }}</td>
                                {{--<td>{{ $p->full_name }}</td>--}}
                                <td>{{ $p->title }}</td>
                                <td>{{ $p->category->name }}</td>
                                <td>{{ $p->created_at }}</td>
                                <td width="130">
                                    <a href="{{ route('admin.posts.edit', $p->id) }}" class="btn btn-sm btn-default">Edit</a>
                                    <a href="{{ route('admin.posts.destroy', $p->id) }}" class="btn btn-sm btn-danger">Remove</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3">No results</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>

                {!! $posts->appends($request->all())->links() !!}

            </div>
        </div>

    </div>
@endsection
