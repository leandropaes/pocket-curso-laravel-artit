@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <h1>
                    Posts <small>\ New</small>
                </h1>
            </div>
        </div>

        <hr>

        @include('admin.utils.errors')

        <div class="row">
            <div class="col-sm-12">

                {!! Form::open(['route' => 'admin.posts.store', 'method' => 'post']) !!}

                    @include('admin.posts.partials.form')

                {!! Form::close() !!}

            </div>
        </div>

    </div>
@endsection
