@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <h1>
                    Posts <small>\ Edit: {{ $post->title }}</small>
                </h1>
            </div>
        </div>

        <hr>

        @include('admin.utils.errors')

        <div class="row">
            <div class="col-sm-12">

                {!! Form::model($post, ['route' => ['admin.posts.update', $post->id], 'method' => 'post']) !!}

                    @include('admin.posts.partials.form')

                {!! Form::close() !!}

            </div>
        </div>

    </div>
@endsection
