@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <h1>
                    Dashboard
                </h1>
            </div>
        </div>

    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/admin/js/modules/dashboard/index.js') }}"></script>
@endsection