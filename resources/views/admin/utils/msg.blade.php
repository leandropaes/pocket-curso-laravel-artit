@if(session('msg'))
    <p class="alert alert-success">{{ session('msg') }}</p>
@endif